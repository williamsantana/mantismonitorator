# README #

Este é um plugin para o Google Chrome 42+ para monitorar as atividades do sistema de bigtracker, Mantis.

Para instalar é simples: 
* Faça o clone do fonte para qualquer diretorio.
* Abra as configuracoes de extensão do Chrome.
* Clique em 'Carregar extensão expandida' ou 'Load unpacked extension'
* Escolha a pasta raiz onde está o clone do projeto
* Pronto, o plugin ja vai estar funcionando.