String.prototype.toTimeStamp = function () {
    var splitted = this.split(":");
    var hour = parseFloat(splitted[0]);
    var minute = parseFloat(splitted[1]);
    var second = parseFloat(splitted[2]);
    return (hour * 3600) + (minute * 60) + second;
};

/* global self */

var Index = (new function () {
    var self = this;
    var sections = [];
    var haveMessages = false;
    var mantisAtualizados = [];
    var verifyInterval = null;
    this.chrome = null;
    this.notificationsOpened = [];

    this.init = function (chrome) {
        self.chrome = chrome;
        self.verifyInterval = 60; // Em segundos
        self.createAlarm();
        self.loadDoc();
    };
    
    this.createAlarm = function () {
        self.chrome.alarms.create('mantis_monitorator_alarm', {periodInMinutes: (self.verifyInterval / 60)});
    };

    this.loadDoc = function (callback) {
        $.ajax({
            url: 'http://www.pdcase.com:81/mantis/my_view_page.php',
            type: 'POST',
            dataType: 'html',
            data: {},
            success: function (html) {
                $('#hiddendoc').html(html);
                self.sections = [];
                self.haveMessages = false;
                self.mantisAtualizados = [];
                self.mantisInterval = 60;

                self.parseSections(html);
                self.killDoc();
                self.verifyMantis();
                if (self.haveMessages) {
                    self.emitMessage();
                }
                else {
                    console.log('Nao ha novos mantis [' + (new Date()).toLocaleString() + ']');
                }
            }
        });
    };

    this.splitSections = function () {
        var secs = $("#hiddendoc").find(".form-title");
        for (var i = 0; i < secs.length; i++) {
            self.sections.push({
                obj: $($(secs.get(i)).parentsUntil('table').parent().get(0)),
                qtd: 0
            });
        }
    };

    this.parseSections = function () {
        self.splitSections();

        for (var i = 0; i < self.sections.length; i++) {
            // Verifica quantidades de mantis
            var obj = $(self.sections[i].obj.find('.form-title').get(0));
            var html = obj.html().replace(/(<([^>]+)>)/ig, "").replace(/\s+/gi, '');
            html = html.substr(html.indexOf('(')).replace(/(\(|\))/gi, '');
            var qtd = html.split('/')[1];
            self.sections[i].qtd = (!isNaN(parseFloat(qtd)) ? parseFloat(qtd) : 0);

            // Faz o parse dos mantis
            var obj = self.sections[i].obj.find('tr');
            self.sections[i].mantis = [];
            for (var j = 0, k = 0; j < obj.length; j++) {
                self.sections[i].mantis[k] = {};
                if (j === 0)
                    continue; // Ignora o primeiro tr que é o cabecalho

                self.sections[i].mantis[k].obj = $(obj.get(j));
                self.sections[i].mantis[k].section = i;
                
                // Parse do id do mantis
                var texto = $(self.sections[i].mantis[k].obj.find('td').get(0)).text().replace(/^\s+|\s+$/gi, '');
                self.sections[i].mantis[k].id = parseFloat(texto);

                var texto = $(self.sections[i].mantis[k].obj.find('td').get(1)).text();
                // Parse do titulo do mantis
                var pars = texto.substr(0, texto.lastIndexOf('-')).replace(/^\s+|\s+$/gi, '').replace(/(?:\r\n|\r|\n)/g, '|');
                var titulo = pars.split('|')[0].replace(/^\s+|\s+$/gi, '');
                var csu = pars.split('|')[1].replace(/^\s+|\s+$/gi, '');
                self.sections[i].mantis[k].titulo = titulo;
                self.sections[i].mantis[k].csu = csu;

                // Parse da data-hora
                var datahora = texto.substr(texto.lastIndexOf('-') + 1).replace(/^\s+|\s+$/gi, '');
                var data = datahora.split(' ')[0];
                var hora = datahora.split(' ')[1];
                self.sections[i].mantis[k].data = data;
                self.sections[i].mantis[k].hora = hora;
                k++;
            }
        }
    };

    this.verifyMantis = function () {
        // Verifica em cada secao, se existem mantis modificados dentro de 1 minuto
        for (var i = 0; i < self.sections.length; i++) {
            if (self.sections[i].qtd > 0) {
                for (var j = 0; j < self.sections[i].mantis.length; j++) {
                    if (self.sections[i].mantis[j].data === self.today()) {
                        if (self.isLastMinute(self.sections[i].mantis[j].hora)) {
                            self.haveMessages = true;
                            self.mantisAtualizados.push(self.sections[i].mantis[j]);
                        }
                    }
                }
            }
        }
    };

    this.killDoc = function () {
        $('#hidden-doc').remove();
    };

    this.emitMessage = function () {
        for (var i = 0; i < self.mantisAtualizados.length; i++) {
            if ($.inArray(self.mantisAtualizados[i].id, self.notificationsOpened) === -1) {
                var titulo = '';
                if (self.mantisAtualizados[i].section === 0) {
                    titulo = 'Novo mantis atribuido a voce';
                }
                else if (self.mantisAtualizados[i].section === 1) {
                    titulo = 'Mantis que voce criou foi modificado';
                }
                else if (self.mantisAtualizados[i].section === 2) {
                    titulo = 'Mantis foi resolvido';
                }
                else if (self.mantisAtualizados[i].section === 3) {
                    titulo = 'Mantis modificado recentemente';
                }
                else if (self.mantisAtualizados[i].section === 4) {
                    titulo = 'Mantis monitorado por voce foi modificado';
                }
                
                self.chrome.notifications.create(
                    'notification-mantis-' + self.mantisAtualizados[i].id, 
                    {
                        type: 'basic', 
                        iconUrl: 'images/icon.png', 
                        title: titulo,
                        message: self.mantisAtualizados[i].csu + ' - ' + self.mantisAtualizados[i].titulo
                    },
                    function() {}
                );
                self.notificationsOpened.push(self.mantisAtualizados[i].id);
            }
        }
    };

    this.today = function () {
        var data = new Date();
        var dia = (data.getDate() < 10 ? '0' + data.getDate() : data.getDate());
        var mes = ((data.getMonth() + 1) < 10 ? '0' + (data.getMonth() + 1) : (data.getMonth() + 1));
        var ano = (data.getFullYear() < 10 ? '0' + data.getFullYear() : data.getFullYear());

        return dia + '/' + mes + '/' + ano;
    };

    this.isLastMinute = function (hora) {
        var hl = new Date();
        var ho = (hl.getHours() < 10 ? '0' + hl.getHours() : hl.getHours());
        var mi = ((hl.getMinutes() + 1) < 10 ? '0' + (hl.getMinutes() + 1) : (hl.getMinutes() + 1));
        var se = (hl.getSeconds() < 10 ? '0' + hl.getSeconds() : hl.getSeconds());
        var now = ho + ':' + mi + ':' + se;

        var hh = hora.toTimeStamp();
        var now = now.toTimeStamp();
        var diff = now - hh;

        return (diff <= self.mantisInterval ? true : false);
    };


    this.diffTime = function (time1, time2) {
        time1 = time1.toTimeStamp();
        time2 = time2.toTimeStamp();
        timeDiff = time1 - time2;
        return timeDiff.toTimeString();
    };

});

$(document).ready(function () {
    chrome.alarms.onAlarm.addListener(function( alarm ) {
        Index.init(chrome);
    });
    chrome.notifications.onClosed.addListener(function (notificationId, byUser) {
        var id = parseFloat(notificationId.replace('notification-mantis-', ''));
        var iof = Index.notificationsOpened.indexOf(id);
        delete Index.notificationsOpened[iof];
    });
    chrome.notifications.onClicked.addListener(function (notificationId) {
        var id = parseFloat(notificationId.replace('notification-mantis-', ''));
        chrome.tabs.create({url : 'http://www.pdcase.com:81/mantis/view.php?id=' + id}, function () {});
    });
    
    Index.init(chrome);
});